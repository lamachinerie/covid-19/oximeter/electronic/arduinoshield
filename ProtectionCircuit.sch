EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Pulsoximiter"
Date ""
Rev "A"
Comp "Alban Petit"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Diode:BAV99 D?
U 1 1 5EE85095
P 5450 1850
AR Path="/5EE85095" Ref="D?"  Part="1" 
AR Path="/5EE82CA9/5EE85095" Ref="D1"  Part="1" 
F 0 "D1" H 5450 2066 50  0000 C CNN
F 1 "BAV99" H 5450 1975 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5450 1975 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 5450 1850 50  0001 C CNN
	1    5450 1850
	1    0    0    -1  
$EndComp
$Comp
L Diode:BAV99 D?
U 1 1 5EE8509B
P 5450 3350
AR Path="/5EE8509B" Ref="D?"  Part="1" 
AR Path="/5EE82CA9/5EE8509B" Ref="D3"  Part="1" 
F 0 "D3" H 5450 3566 50  0000 C CNN
F 1 "BAV99" H 5450 3475 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5450 3566 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 5450 3475 50  0001 C CNN
	1    5450 3350
	1    0    0    -1  
$EndComp
$Comp
L Diode:BAV99 D?
U 1 1 5EE850A1
P 5450 4100
AR Path="/5EE850A1" Ref="D?"  Part="1" 
AR Path="/5EE82CA9/5EE850A1" Ref="D4"  Part="1" 
F 0 "D4" H 5450 4316 50  0000 C CNN
F 1 "BAV99" H 5450 4225 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5450 4225 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 5450 4100 50  0001 C CNN
	1    5450 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1850 4750 1850
Wire Wire Line
	4750 1850 4750 2600
$Comp
L power:GND #PWR?
U 1 1 5EE850A9
P 4750 4550
AR Path="/5EE850A9" Ref="#PWR?"  Part="1" 
AR Path="/5EE82CA9/5EE850A9" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 4750 4300 50  0001 C CNN
F 1 "GND" H 4755 4377 50  0000 C CNN
F 2 "" H 4750 4550 50  0001 C CNN
F 3 "" H 4750 4550 50  0001 C CNN
	1    4750 4550
	1    0    0    -1  
$EndComp
Connection ~ 4750 2600
Wire Wire Line
	4750 2600 4750 3350
Wire Wire Line
	5150 3350 4750 3350
Connection ~ 4750 3350
Wire Wire Line
	5450 2050 5450 2200
Wire Wire Line
	5450 3550 5450 3700
Wire Wire Line
	4750 3350 4750 4100
Wire Wire Line
	5150 4100 4750 4100
Connection ~ 4750 4100
Wire Wire Line
	4750 4100 4750 4550
Wire Wire Line
	5750 3350 6000 3350
Wire Wire Line
	6000 3350 6000 4100
Wire Wire Line
	6000 4100 5750 4100
Wire Wire Line
	5750 1850 6000 1850
Wire Wire Line
	6000 1850 6000 2600
Wire Wire Line
	6000 2600 5750 2600
Connection ~ 6000 3350
Wire Wire Line
	6000 1850 6700 1850
Connection ~ 6000 1850
Wire Wire Line
	5450 4450 6700 4450
Wire Wire Line
	5450 3700 6700 3700
Wire Wire Line
	5450 2950 6700 2950
Wire Wire Line
	5450 2200 6700 2200
Wire Wire Line
	5150 2600 4750 2600
$Comp
L Diode:BAV99 D?
U 1 1 5EE850CC
P 5450 2600
AR Path="/5EE850CC" Ref="D?"  Part="1" 
AR Path="/5EE82CA9/5EE850CC" Ref="D2"  Part="1" 
F 0 "D2" H 5450 2816 50  0000 C CNN
F 1 "BAV99" H 5450 2725 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5450 2725 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BAV99_SER.pdf" H 5450 2600 50  0001 C CNN
	1    5450 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4300 5450 4450
Wire Wire Line
	5450 2800 5450 2950
Wire Wire Line
	6000 3350 6700 3350
Wire Notes Line
	600  7700 6850 7700
Wire Notes Line
	6850 7700 6850 5600
Wire Notes Line
	6850 5600 600  5600
Wire Notes Line
	600  5600 600  7700
Text Notes 3450 5800 0    50   ~ 10
Informations\n
Text HLabel 6700 4450 2    50   Input ~ 0
TXP
Text HLabel 6700 3700 2    50   Input ~ 0
TXN
Text HLabel 6700 2950 2    50   Input ~ 0
INP
Text HLabel 6700 2200 2    50   Input ~ 0
INN
Text HLabel 6700 1850 2    50   Input ~ 0
RX_ANA_SUP
Text HLabel 6700 3350 2    50   Input ~ 0
LED_DRV_SUP
$EndSCHEMATC
